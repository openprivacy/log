# log

A simple log primative Open Privacy uses

## Examples:

    log.AddEverythingFromPattern("connectivity")
    log.SetLevel(log.LevelDebug)
    log.ExcludeFromPattern("connection/connection")
    log.ExcludeFromPattern("outbound/3dhauthchannel")
    log.AddPrivacyFilter(func(s string) bool {
        return len(s) == 56
    })
    log.SetPrivacyFilterReplace(true)

    log.Debugf("Creating the thing with param %v\n", param)
    log.Infoln("Doing thing X")
    log.Warnf("This might be bad: %v\n", err)
    log.Errorf("Failed to do thing: %v\n", err)

### Making log go to a file

```
if runtime.GOOS == "windows" {
	filelogger, err := log.NewFile(log.LevelInfo, "cwtch_log.txt")
	if err == nil {
		log.SetStd(filelogger)
	}
}
```
